
/**
 * Dependencies
 * @ignore
 */
import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Card } from 'react-bootstrap';
import './App.css';
import { withRouter } from 'react-router-dom';
/**
 * App
 * @ignore
 */
class App extends Component {
  //en metode for å hindre async
  constructor(props) {
    super(props);
    this.state = {
      name: null,
      username: null,
      avatar_url: null,
    }
  }

  componentDidMount() {
    this.asyncLoad().then()
  }

  async asyncLoad() {
    //await new Promise(resolve => setTimeout(() => resolve(), 3000))

    const { location } = this.props
    if (location.hash) {
      const access_token = [...new URLSearchParams(location.hash.substring(1)).entries()][0][1]
      fetch(`https://gitlab.com/api/v4/user`, {
        headers: {
          'Authorization': `Bearer ${access_token}`
        }
      }).then((response) => { return response.json() }).then((data) => {
        this.state.name = data.name
        this.state.username = data.username
        this.state.avatar_url = data.avatar_url
        this.forceUpdate()
      });
      return console.log('LOCATION', [...new URLSearchParams(location.hash.substring(1)).entries()])
    } else {

      const params = new URLSearchParams({
        redirect_uri: window.location.href.slice(0, -1),
        client_id: 'aee2ec9fadcda5013808241cc2e08e245a810d0e22af303a2cbed315cf41312d',
        response_type: 'token'
      })

      window.location = `https://gitlab.com/oauth/authorize?${params.toString()}`
    }
  }

  render() {

    return (
      <Card className="profile-card">
        <Card.Img variant="top" src={this.state.avatar_url} />
        <Card.Body>
          <Card.Text>
            Profile: <br />
            Name: {this.state.name} <br />
            Username: {this.state.username}
          </Card.Text>
        </Card.Body>
      </Card>
    )
  }
}

export default withRouter(App)