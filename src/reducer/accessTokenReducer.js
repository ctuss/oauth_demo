const type_string = 'SEC_ACCESS_TOKEN'

export { type_string as type }

export default function(state = null, action = {}) {
    const { type, access_token } = action

    if(type === type_string && access_token) {
        return access_token
    }

    return state
}